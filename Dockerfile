FROM maven:3-jdk-8
RUN apt-get update && apt-get install -y mpich r-cran-rserve
RUN R -e 'install.packages(c("cluster","clv","igraph","fields","e1071","fpc","sprint","kohonen","kernlab","lars","MASS","mlbench","Hmisc","proxy","clusterGeneration","tnet"), repos="http://cran.rstudio.com/")'
# dependencies for densitycut
RUN R -e 'install.packages(c("ape","FNN","RcppAnnoy"), repos="http://cran.rstudio.com/")'
# download densitycut source package
WORKDIR /
RUN wget https://bitbucket.org/jerry00/densitycut_dev/get/v0.01.tar.gz
RUN R -e 'install.packages("/v0.01.tar.gz", repos = NULL, type="source")'
RUN apt-get install -y apt-utils 
RUN apt-get install -y postgresql
USER postgres

# Note: The official Debian and Ubuntu images automatically ``apt-get clean``
# after each ``apt-get``

# Run the rest of the commands as the ``postgres`` user created by the ``postgres-9.3`` package when it was ``apt-get installed``
USER postgres

# Create a PostgreSQL role named ``docker`` with ``docker`` as the password and
# then create a database `docker` owned by the ``docker`` role.
# Note: here we use ``&&\`` to run commands one after the other - the ``\``
#       allows the RUN command to span multiple lines.
RUN    /etc/init.d/postgresql start &&\
    psql --command "CREATE USER clustevalwrite WITH SUPERUSER PASSWORD 'clustevalwrite';" &&\
    createdb -O clustevalwrite clusteval

# Adjust PostgreSQL configuration so that remote connections to the
# database are possible.
#RUN echo "host all  all    0.0.0.0/0  md5" >> /etc/postgresql/9.6/main/pg_hba.conf

# add Rserv.conf
ADD Rserv.conf /etc/Rserve.conf

#RUN echo "listen_addresses='*'" >> /etc/postgresql/9.6/main/postgresql.conf
USER root
